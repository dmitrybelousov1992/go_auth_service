package db

import (
	"context"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const ()

var ErrNoMatch = fmt.Errorf("no matching users")

type Database struct {
	Collection *mongo.Collection
	Client     *mongo.Client
}

func Initialize() (Database, error) {
	db := Database{}

	dbUser, dbPassword, dbName :=
		os.Getenv("MONGO_USER"),
		os.Getenv("MONGO_PASSWORD"),
		os.Getenv("MONGO_URI")

	credential := options.Credential{
		Username: dbUser,
		Password: dbPassword,
	}

	clientOpts := options.Client().ApplyURI(dbName).
		SetAuth(credential)
	client, err := mongo.Connect(context.TODO(), clientOpts)

	if err != nil {
		log.Println("Database connection error: ", err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
	}
	users := client.Database("authdb").Collection("users")

	db.Collection = users
	db.Client = client

	fmt.Println("Connected to MongoDB!")
	return db, nil
}
