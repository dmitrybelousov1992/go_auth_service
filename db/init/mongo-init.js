db.auth('root', 'rootpassword');

db = db.getSiblingDB('authdb');

db.createCollection('users');

db.users.insertMany([
    {
        _id: new ObjectId(),
        username: "Bob",
        password: "bob123",
        authorities: "ADMIN"
    },
    {
        _id: new ObjectId(),
        username: "jon",
        password: "jon123",
        authorities: "USER"
    },
    {
        _id: new ObjectId(),
        username: "Rob",
        password: "Rob123",
        authorities: "ADMIN"
    }
]);