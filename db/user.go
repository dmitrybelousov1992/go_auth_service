package db

import (
	"bitbucket.org/dmitrybelousov1992/go_auth_service/model"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
)

func (db Database) AddUser(user *model.User) error {
	insertResult, err := db.Collection.InsertOne(context.TODO(), user)
	if err != nil {
		return err
	}

	fmt.Println("Inserted a user id: ", insertResult.InsertedID)
	return nil
}

func (db Database) FindAll() (*model.UserList, error) {
	usersList := &model.UserList{}
	filter := bson.D{{}}

	cursor, err := db.Collection.Find(context.TODO(), filter)
	if err != nil {
		return usersList, err
	}

	for cursor.Next(context.TODO()) {
		var elem model.User
		err := cursor.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		usersList.Users = append(usersList.Users, elem)
	}

	if err := cursor.Err(); err != nil {
		log.Fatal(err)
	}

	// Close the cursor once finished
	cursor.Close(context.TODO())

	fmt.Printf("Found users: %+v\n", usersList)

	return usersList, nil
}

func (db Database) FindByUsername(username string) (model.User, error) {
	filter := bson.D{{"username", username}}
	var result model.User

	switch err := db.Collection.FindOne(context.TODO(), filter).Decode(&result); err {
	case mongo.ErrNoDocuments:
		return result, ErrNoMatch
	default:
		fmt.Printf("Found a single user: %+v\n", result)
		return result, err
	}
}

func (db Database) FindByUsernameAndPassword(user model.User) (model.User, error) {
	filter := bson.D{
		{"username", user.Username},
		{"password", user.Password},
	}
	var result model.User

	switch err := db.Collection.FindOne(context.TODO(), filter).Decode(&result); err {
	case mongo.ErrNoDocuments:
		return result, ErrNoMatch
	default:
		fmt.Printf("Found a single user: %+v\n", result)
		return result, err
	}
}

func (db Database) FindById(id primitive.ObjectID) (model.User, error) {
	filter := bson.D{{"_id", id}}
	var result model.User

	switch err := db.Collection.FindOne(context.TODO(), filter).Decode(&result); err {
	case mongo.ErrNoDocuments:
		return result, ErrNoMatch
	default:
		fmt.Printf("Found a single user: %+v\n", result)
		return result, err
	}
}

func (db Database) UpdateUser(user *model.User) error {
	filter := bson.D{{"_id", user.ID}}

	update := bson.M{
		"$set": user,
	}
	updateResult, err := db.Collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return err
	}

	fmt.Printf("Matched - %+v user and updated - %+v users.\n", updateResult.MatchedCount, updateResult.MatchedCount)
	return nil
}

func (db Database) DeleteUser(id primitive.ObjectID) error {
	filter := bson.D{{"_id", id}}
	deleteResult, err := db.Collection.DeleteOne(context.TODO(), filter)
	if err != nil {
		return err
	}
	fmt.Printf("Deleted %v user in the users collection\n", deleteResult.DeletedCount)
	return nil
}
