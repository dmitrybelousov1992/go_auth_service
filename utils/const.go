package utils

const (
	ACCESS_TOKEN      = "ACCESS_SECRET"
	REFRESH_TOKEN     = "REFRESH_SECRET"
	ACCESS_CLAIM_KEY  = "access_uuid"
	REFRESH_CLAIM_KEY = "refresh_uuid"
)
