package redis

import (
	"bitbucket.org/dmitrybelousov1992/go_auth_service/model"
	"os"
	"time"

	"github.com/go-redis/redis/v7"
)

var client *redis.Client

func Init() {
	dsn := os.Getenv("REDIS_DSN")
	client = redis.NewClient(&redis.Options{
		Addr: dsn,
	})
	_, err := client.Ping().Result()
	if err != nil {
		panic(err)
	}
}

func CreateAuth(userid string, td *model.TokenDetails) error {
	at := time.Unix(td.AtExpires, 0) //converting Unix to UTC(to Time object)
	rt := time.Unix(td.RtExpires, 0)
	now := time.Now()

	errAccess := client.Set(td.AccessUuid, userid, at.Sub(now)).Err()
	if errAccess != nil {
		return errAccess
	}
	errRefresh := client.Set(td.RefreshUuid, userid, rt.Sub(now)).Err()
	if errRefresh != nil {
		return errRefresh
	}
	return nil
}

func FetchAuth(authD *model.AccessDetails) (string, error) {
	userid, err := client.Get(authD.Uuid).Result()
	if err != nil {
		return "", err
	}
	return userid, nil
}

func DeleteAuth(givenUuid string) (int64, error) {
	deleted, err := client.Del(givenUuid).Result()
	if err != nil {
		return 0, err
	}
	return deleted, nil
}
