package main

import (
	"bitbucket.org/dmitrybelousov1992/go_auth_service/controller"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/db"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/redis"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/service"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"log"
)

func loadEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
}

func main() {
	loadEnv()

	database, err := db.Initialize()
	if err != nil {
		log.Fatal(err)
	}

	service.NewHandler(database)
	controller.InitAuth(database)
	redis.Init()
	router := gin.Default()

	router.POST("/login", controller.Login)
	router.GET("/checkToken", controller.TokenAuthVerify(), controller.CheckToken)
	router.GET("/logout", controller.TokenAuthVerify(), controller.Logout)
	router.POST("/refresh", controller.TokenAuthVerify(), controller.Refresh)
	router.POST("/registration", service.PostUser)

	router.Run("localhost:8080")
}
