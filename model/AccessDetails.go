package model

type AccessDetails struct {
	Uuid   string
	UserId string
}
