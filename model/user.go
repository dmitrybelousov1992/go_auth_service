package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	ID          primitive.ObjectID `bson:"_id" json:"_id"`
	Username    string             `json:"username"`
	Password    string             `json:"password"`
	Authorities string             `json:"authorities"`
}

type UserList struct {
	Users []User `json:"users"`
}
