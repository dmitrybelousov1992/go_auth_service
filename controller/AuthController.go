package controller

import (
	"bitbucket.org/dmitrybelousov1992/go_auth_service/db"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/jwt"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/model"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/redis"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/utils"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

var dbInstance db.Database

func InitAuth(db db.Database) {
	dbInstance = db
}

func Login(c *gin.Context) {
	var userModel model.User
	if err := c.BindJSON(&userModel); err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	user, err := dbInstance.FindByUsernameAndPassword(userModel)
	if err != nil {
		if err == db.ErrNoMatch {
			c.JSON(http.StatusBadRequest, "Invalid login or password")
			return
		}
		c.JSON(http.StatusInternalServerError, "")
		return
	}
	token, err := jwt.CreateToken(user.ID.Hex())
	if err != nil {
		c.JSON(http.StatusInternalServerError, "")
		return
	}

	saveErr := redis.CreateAuth(user.ID.Hex(), token)
	if saveErr != nil {
		c.JSON(http.StatusUnprocessableEntity, saveErr.Error())
		return
	}
	tokens := map[string]string{
		"access_token":  token.AccessToken,
		"refresh_token": token.RefreshToken,
	}
	c.IndentedJSON(http.StatusOK, tokens)
}

func CheckToken(c *gin.Context) {
	tokenString := extractToken(c.Request)

	accessDetails, err := jwt.ExtractTokenMetadata(tokenString, utils.ACCESS_TOKEN)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	userId, err := redis.FetchAuth(accessDetails)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	c.IndentedJSON(http.StatusOK, strings.Compare(accessDetails.UserId, userId) == 0)
}

func Logout(c *gin.Context) {
	tokenString := extractToken(c.Request)
	accessDetails, err := jwt.ExtractTokenMetadata(tokenString, utils.ACCESS_TOKEN)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	deleted, delErr := redis.DeleteAuth(accessDetails.Uuid)
	if delErr != nil || deleted == 0 { //if any goes wrong
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	c.JSON(http.StatusOK, "Successfully logged out")
}

func Refresh(c *gin.Context) {
	mapToken := map[string]string{}
	if err := c.ShouldBindJSON(&mapToken); err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	refreshToken := mapToken["refresh_token"]
	accessDetails, err := jwt.ExtractTokenMetadata(refreshToken, utils.REFRESH_TOKEN)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}

	deleted, delErr := redis.DeleteAuth(accessDetails.Uuid)
	if delErr != nil || deleted == 0 { //if any goes wrong
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	//Create new pairs of refresh and access tokens
	ts, createErr := jwt.CreateToken(accessDetails.UserId)
	if createErr != nil {
		c.JSON(http.StatusForbidden, createErr.Error())
		return
	}
	//save the tokens metadata to redis
	saveErr := redis.CreateAuth(accessDetails.UserId, ts)
	if saveErr != nil {
		c.JSON(http.StatusForbidden, saveErr.Error())
		return
	}
	tokens := map[string]string{
		"access_token":  ts.AccessToken,
		"refresh_token": ts.RefreshToken,
	}
	c.JSON(http.StatusCreated, tokens)
}

func TokenAuthVerify() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := extractToken(c.Request)
		err := jwt.TokenValid(tokenString, utils.ACCESS_TOKEN)
		if err != nil {
			c.JSON(http.StatusUnauthorized, err.Error())
			c.Abort()
			return
		}
		c.Next()
	}
}

func extractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}
