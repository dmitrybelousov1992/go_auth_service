package service

import (
	"bitbucket.org/dmitrybelousov1992/go_auth_service/db"
	"bitbucket.org/dmitrybelousov1992/go_auth_service/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

var dbInstance db.Database

func NewHandler(db db.Database) {
	dbInstance = db
}

func GetUsers(c *gin.Context) {
	users, err := dbInstance.FindAll()
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}

	c.IndentedJSON(http.StatusOK, users)
}

func PostUser(c *gin.Context) {
	var newUser model.User
	if err := c.BindJSON(&newUser); err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	newUser.ID = primitive.NewObjectID()
	err := dbInstance.AddUser(&newUser)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}
	c.IndentedJSON(http.StatusOK, "")
}

func GetUsersByID(c *gin.Context) {
	id := c.Param("id")

	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("Invalid id")
	}

	user, err := dbInstance.FindById(objectId)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err)
	}

	c.IndentedJSON(http.StatusOK, user)

}
